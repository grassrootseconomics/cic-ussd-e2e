# cic-ussd-e2e

Automate traversing USSD menu's with little human interaction and as close replication to actual USSD usage including AT style reqests and waiting times. Useful for simulation load on the USSD server.

## Setup

Inside repo:

```
# Install dependencies.
$ npm install

# (Optional) Link bin.
$ npm link

# (Optional) Run program directly.
$ ./bin/cic-ussd-e2e
```

## Usage

**Environment variables:**:

```bash
# Service code.
SERVICE_CODE=*483*061#

# HTTP endpoint exposed by USSD server.
ENDPOINT=https://ussd.grassecon.net

# No. of ms to wait before moving to next menu.
DEFAULT_WAIT_NEXT_MENU=3000
```

**Running:**

```bash
./bin/cic-ussd-e2e ./specs/01-setup-account/choose-language.spec
```

## Spec file

**Example:**

```
# phone +254700000001

< Balance

# wait

> 1

< My Account
```

**Description:**

A spec file represents a single end-to-end test mimicing user
navigating menu by menu providing different inputs.

There are 3 types of scenes:

1. **command** scene (marker: `#`):
  An instruction is given to the e2e test runner.

  Available commands include:
  1. `phone <num>`: sets current phone no. and "dials" USSD.
  1. `wait <ms>`: waits for some time before proceeding with test.

2. **input** scene (marker: `>`):
  Input sent to USSD server mimicing user interaction.

3. **output** scene (marker: `<`):
  Strings to expect to find in the current menu.

### Limitations

- This tool cannot reset USSD sessions upon failure (stops test on failure). You will need to manually set your account USSD menu to the main menu
- Cannot verify sms status, confirm with cic-notify for delivery
