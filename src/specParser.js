const process = require("process");
const resolve = require("path").resolve;
const readFile = require("fs").readFileSync;

const {
  MARKER_COMMAND,
  MARKER_INPUT,
  MARKER_OUTPUT,
} = require("./constants");

function parser(specFile) {
  const rawData = readFile(resolve(process.cwd(), specFile), "utf8");
  const lines = rawData.split("\n");

  const scenes = [];
  let cursor;

  const addScene = () => {
    if (cursor.marker === MARKER_COMMAND) {
      const commandStr = cursor.lines.join(" ").trim();
      const command = commandStr.split(" ")[0];
      scenes.push({
        command,
        content: commandStr.slice(command.length + 1),
      });
      resetCursor();
      return;
    }
    scenes.push({
      content: cursor.lines,
      input: cursor.marker === MARKER_INPUT,
      output: cursor.marker === MARKER_OUTPUT,
    });
    resetCursor();
  };

  const identifyLine = (marker, content) => {
    switch (marker) {
      case MARKER_COMMAND:
        const command = content.split(" ")[0];
        return `command:${command}`;
      case MARKER_INPUT:
        return `input`;
      case MARKER_OUTPUT:
        return `output`;
    }
  };

  const resetCursor = () => {
    cursor = {
      id: null,
      lines: [],
      marker: null,
    };
  };

  resetCursor();
  for (let line of lines) {
    line = line.trim();
    if (!line.length) {
      continue;
    }
    const marker = line[0];
    const content = line.slice(1).trim();
    const id = identifyLine(marker, content);
    if (cursor.id && cursor.id !== id) {
      addScene();
    }
    cursor.id = id;
    cursor.lines.push(content);
    cursor.marker = marker;
  }
  addScene();

  return scenes;
}

module.exports = { parser };
