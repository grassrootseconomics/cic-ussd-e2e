const jobAllocator = require("./jobAllocator").jobAllocator;

function cli(args) {
  jobAllocator(args.slice(2));
}

module.exports = { cli };
