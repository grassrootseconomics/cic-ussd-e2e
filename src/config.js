module.exports = {
  ussd: {
    serviceCode: process.env.SERVICE_CODE || "*483*061#",
    endpoint: process.env.ENDPOINT || "https://ussd.grassecon.net",
    timeout: 3000,
  },
  defaultWaitNextMenu: parseInt(process.env.DEFAULT_WAIT_NEXT_MENU || '3000', 10),
};
