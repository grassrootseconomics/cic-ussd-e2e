const path = require("path");

const rangi = require("rangi");
const Piscina = require("piscina");

const parser = require("./specParser").parser;

const piscina = new Piscina({
  filename: path.resolve(__dirname, "worker.js"),
  // thread configs?
});

async function jobAllocator(specFiles) {
  console.log(
    rangi.yellow(`[*] Number of workers: ${specFiles.length}`)
  );

  const parallelExecutor = [];

  for (let i = 0; i < specFiles.length; i++) {
    parallelExecutor.push(
      piscina.run({
        jobId: i,
        scenes: parser(specFiles[i]),
      })
    );
  }

  await Promise.all(parallelExecutor);
}

module.exports = { jobAllocator };
