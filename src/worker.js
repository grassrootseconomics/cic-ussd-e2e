const crypto = require("crypto");
const setTimeout = require("timers/promises").setTimeout;
const rangi = require("rangi");
const phin = require("phin");

const config = require("./config");
const {
  COMMAND_PHONE,
  COMMAND_REDIAL,
  COMMAND_WAIT,
} = require("./constants");

const cursor = {
  output: null,
  phone: null,
};

const commands = {
  [COMMAND_PHONE]: async (phone) => {
    console.log(
      rangi.yellow(
        `[*] Setting phone number to ${phone}`,
      ),
    );
    cursor.phone = phone;
    await sendRequest("");
  },
  [COMMAND_REDIAL]: async () => {
    console.log(
      rangi.yellow(
        `[*] Redialling ussd via phone number ${cursor.phone}`,
      ),
    );
    await sendRequest("");
  },
  [COMMAND_WAIT]: async (secs) => {
    console.log(
      rangi.yellow(
        `[*] Waiting ${secs} secs before carrying out next action`,
      ),
    );
    await setTimeout(parseInt(secs, 10) * 1e3);
  },
};
const sessions = {};

function registerSession() {
  sessions[cursor.phone] = crypto.randomBytes(16).toString("hex");
}

async function sendRequest(input) {
  console.log(
    rangi.yellow(
      `[*] Sending input '${input}' to ussd server at ${config.ussd.endpoint}`,
    ),
  );
  await setTimeout(config.defaultWaitNextMenu);
  if (!sessions[cursor.phone]) {
    registerSession();
  }
  const requestOptions = {
    url: config.ussd.endpoint,
    method: "POST",
    parse: "string",
    timeout: config.ussd.timeout,
    form: {
      sessionId: sessions[cursor.phone],
      phoneNumber: cursor.phone,
      serviceCode: config.ussd.serviceCode,
      text: input,
    },
  };
  let { body } = await phin(requestOptions);
  body = body.length > 0 ? body : null;
  if (!body) {
    return null;
  }
  if (body.slice(0, 4) === "END ") {
    registerSession();
  }
  console.log(rangi.cyan(body.split("\n").map(i => `< ${i}`).join("\n")));
  cursor.output = body;
}

module.exports = async ({ jobId, scenes }) => {
  for (const scene of scenes) {
    if (scene.command) {
      const args = scene.content.split(" ");
      await commands[scene.command](...args);
    } else if (scene.input) {
      const input = scene.content.join("\n").trim();
      console.log(rangi.blue(`> ${input}`));
      await sendRequest(input);
    } else if (scene.output) {
      for (const line of scene.content) {
        const found = cursor.output?.includes(line);
        if (!found) {
          throw new Error(`String '${line}' not found`);
        }
      }
    }
  }
  console.log(rangi.green('[*] Passed'));
};
