const constants = {};

constants.COMMAND_PHONE = "phone";
constants.COMMAND_REDIAL = "redial";
constants.COMMAND_WAIT = "wait";

constants.MARKER_COMMAND = "#";
constants.MARKER_INPUT = ">";
constants.MARKER_OUTPUT = "<";

exports = module.exports = constants;
